import 'package:flame/components.dart';
import 'package:flame/extensions.dart';

class StepTime {
  static const List<double> sword = [0.2, 0.1, 0.1, 0.1, 0.3];
  static const List<double> knife = [0.2, 0.1, 0.1, 0.1, 0.3];
}

class TextureSize {
  static final Vector2 sword = Vector2(260, 360);
  static final Vector2 knife = Vector2(260, 360);
}

class CharConfig {
  static final Vector2 characterSize = Vector2(80, 120);
}
