import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'entity/player/player.dart';
import 'new_lodge.dart';

Widget testAnimationHudBuilder(BuildContext ctx, NewLodge game) {
  int index = 0;
  return Scaffold(
    backgroundColor: Colors.transparent,
    body: Container(
      width: MediaQuery.of(ctx).size.width,
      height: MediaQuery.of(ctx).size.height,
      color: Colors.transparent,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  margin: EdgeInsets.all(30),
                  width: 150,
                  height: 150,
                ),
                IconButton(
                  icon:
                      Image.asset('assets/images/hud/elements/button_test.png'),
                  iconSize: 150,
                  onPressed: () {
                    print(index);
                    switch (index) {
                      case 0:
                        // game.playerState = PlayerState.stand;
                        return;
                      case 1:
                        break;
                      case 2:
                        break;
                    }
                  },
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  iconSize: 50,
                  onPressed: () {
                    // game.playerState = PlayerState.stand;
                    index = 0;
                  },
                  icon: SvgPicture.asset(
                    'assets/icons/hand_bones.svg',
                  ),
                  highlightColor: Colors.blue,
                ),
                IconButton(
                  iconSize: 50,
                  onPressed: () {
                    // game.playerState = PlayerState.knife;
                    index = 1;
                  },
                  icon: SvgPicture.asset(
                    'assets/icons/surgical.svg',
                  ),
                ),
                IconButton(
                  iconSize: 50,
                  color: Colors.black,
                  onPressed: () {
                    // game.playerState = PlayerState.sword;
                    index = 2;
                  },
                  icon: SvgPicture.asset(
                    'assets/icons/swords.svg',
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ),
  );
}

// class TestingHud extends PositionComponent with HasGameRef<NewLodge> {
//   TestingHud({
//     super.position,
//     super.size,
//     super.scale,
//     super.angle,
//     super.anchor,
//     super.children,
//     super.priority = 5,
//   });
//
//   late final TextComponent _scoreTextComponent;
//
//   // @override
//   // Future<void> onLoad() async {
//   //   _scoreTextComponent = TextComponent(
//   //     text: 'Highscore: ${game.highScore} Game Score: ${game.score}',
//   //     textRenderer: TextPaint(
//   //       style: const TextStyle(
//   //         fontSize: 18,
//   //         color: Color.fromRGBO(10, 10, 10, 1),
//   //       ),
//   //     ),
//   //     anchor: Anchor.topCenter,
//   //     position: Vector2(game.size.x / 2, 20),
//   //   );
//   //   add(_scoreTextComponent);
//   // }
// }
