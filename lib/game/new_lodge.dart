import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame_tiled/flame_tiled.dart';

import '../settings.dart';
import 'entity/player/player.dart';
import 'hud/controls/joystick.dart';
import 'input.dart';

class NewLodge extends FlameGame with HasCollisionDetection {
  late TiledComponent map;
  late final CameraComponent cameraComponent;
  late final JoystickComponent joystick;

  late final Player player;
  int testIndex = 0;

  NewLodge() : super();

  @override
  Future<void> onLoad() async {
    // load all images
    await images.loadAll([
      Animations.sword,
      Animations.knife,
      Animations.stand,
      CharImg.TEST_CHARACTER,
    ]);

    cameraComponent = CameraComponent.withFixedResolution(
      width: size.x,
      height: size.y,
      world: world,
    );

    cameraComponent.viewport.size = Vector2(size.x, size.y);
    map = await TiledComponent.load('map/map_23x23.tmx', Vector2.all(80));
    world.add(map);
    map.anchor = Anchor.center;
    player = Player();
    joystick = await getJoystick();
    world.addAll([player, joystick]);
    final tab = MyTabActionClass();
    // world.add(tab);
    add(tab);

    // Sword sword = Sword();
    // world.add(sword);
  }

  @override
  void update(dt) {
    // print('current state: $playerState');
    //
    // player.update(dt);

    super.update(dt);
  }
}
