import 'dart:math';

import 'package:flame/components.dart';
import 'package:flutter/material.dart';

import '../../../settings.dart';

extension JoystickController on JoystickComponent {
  Direction evaluateInput() {
    // min drag amount to be recognize as user input
    if (intensity < 0.25) return Direction.none;

    final xAbs = delta.x.abs();
    final yAbs = delta.y.abs();

    final a = atan(yAbs / xAbs);

    // horizontal
    if (a < 22.5) {
      return (delta.x > 0) ? Direction.east : Direction.west;
      // vertical
    } else if (a > 67.5) {
      return (delta.y > 0) ? Direction.north : Direction.south;
      // diagonal
    } else {
      return (delta.x > 0)
          ? ((delta.y > 0) ? Direction.northeast : Direction.southeast)
          : ((delta.y > 0) ? Direction.northwest : Direction.southwest);
    }

    // // one of eight directions
    // return (a < 22.5 || a > 67.5)
    //     // cardinal direction
    //     ? ((delta.x > 0) ? ())
    //     // ordinal direction
    //     : ();
    //
    // if (a > 45) {
    //   print("over 45 deg");
    //   return (xAbs > yAbs)
    //       ? ((delta.x > 0) ? Direction.right : Direction.left)
    //       : ((delta.y > 0) ? Direction.down : Direction.up);
    // } else {
    //   print("uner 45 deg");
    //   return (xAbs > yAbs)
    //       ? ((delta.x > 0) ? Direction.right : Direction.left)
    //       : ((delta.y > 0) ? Direction.down : Direction.up);
    // }
  }
}

Future<JoystickComponent> getJoystick() async {
  return JoystickComponent(
    knob: await _getJoystickKnob(null),
    background: await _getJoystickBackground(null),
    knobRadius: 15,
    margin: const EdgeInsets.only(left: 50, bottom: 60),
  );
}

Future<SpriteComponent> _getJoystickKnob(Vector2? position) async {
  final Sprite sprite = await Sprite.load(JoystickImg.knob);
  final size = Vector2.all(55);
  return SpriteComponent(
    sprite: sprite,
    size: size,
    position: position,
  );
}

Future<SpriteComponent> _getJoystickBackground(Vector2? position) async {
  final Sprite sprite = await Sprite.load(JoystickImg.background);
  final size = Vector2.all(90);
  return SpriteComponent(
    sprite: sprite,
    size: size,
    position: position,
  );
}
