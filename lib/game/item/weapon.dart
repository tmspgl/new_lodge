import 'item.dart';

abstract class Weapon extends Item {
  int damage;

  Weapon(String name, int value, this.damage) : super(name, value);

  void attack();
}
