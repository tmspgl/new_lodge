import '../weapon.dart';

class Sword extends Weapon {
  Sword() : super('Sword', 10, 15);

  @override
  void attack() {
    print('$name attack with $damage damage!');
  }

  @override
  void use() {
    // TODO: implement use
  }
}

// class Sword extends AnimatedPlayerItem {
//   Sword(super.cachedImg, super.stepTimes, super.textureSize, super.anchorPoint,
//       super.pos, super.loop);
//
//   @override
//   void onLoad() {
//     cachedImg = Animations.sword;
//     size = CharConfig.characterSize;
//     super.onLoad();
//   }
// }

// final String cachedImg;
//   final List<double> stepTimes;
//   final Vector2 textureSize;
//   final Vector2 anchorPoint;
//   final bool loop;
//   final List<Image> spriteImages;
