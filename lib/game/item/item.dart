abstract class Item {
  String name;
  int value;

  Item(this.name, this.value);

  void use();
}