abstract class Entity extends Assailable {
  String name;
  int health;

  Entity(this.name, this.health);

  @override
  void takeDmg(int damage) {
    health -= damage;
    print('$name took $damage damage. Current health: $health');
  }
}

abstract class Assailable {
  void takeDmg(int damage);
}
//
// // mixins.dart
// mixin M2 { }
// mixin M1 {} includes M2 { }
//
// // a.dart
// class A extends Object with M1 { }
//
// We could desugar the above into the following:
//
// // mixins.dart
// mixin M2 { }
// mixin M1 on M2 { }
//
// // a.dart
// class A extends Object with M1, M2 { }
