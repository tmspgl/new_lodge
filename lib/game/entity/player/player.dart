import 'dart:ui';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:new_lodge/game/entity/entity.dart';

import '../../../settings.dart';
import '../../new_lodge.dart';

enum Orientation { east, south, west, north }

class Player extends SpriteAnimationGroupComponent<Orientation>
    with CollisionCallbacks, HasGameRef<NewLodge> {
  Player() : super() {
    // Initialize your player's properties and animations here
  }

  void useItem() {
    // Implement logic for using items here
  }

  void showItemAnimation() {
    // Implement logic for showing item animations here
  }

  @override
  Future<void> onLoad() async {
    // required int amount,
    //     required List<double> stepTimes,
    //     required Vector2 textureSize,
    //     int? amountPerRow,
    //     Vector2? texturePosition,
    //     this.loop = true,
    final animationData = SpriteAnimationData.sequenced(
      amount: 4,
      stepTime: 0.3,
      textureSize: Vector2(230, 300),
    );

    final anima = SpriteAnimation.fromFrameData(
      game.images.fromCache(Animations.playerStand),
      animationData,
    );

    current = Orientation.south;

    anchor = Anchor.center;

    add(RectangleHitbox());

    animations = {
      Orientation.north: anima,
      Orientation.east: anima,
      Orientation.south: anima,
      Orientation.west: anima,
    };
  }

  @override
  void update(double dt) {
    super.update(dt);
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
  }
}
//
// enum PlayerState {
//   sword,
//   knife,
//   stand,
// }

// class Player2 extends SpriteAnimationGroupComponent<PlayerState>
//     with CollisionCallbacks, HasGameRef<NewLodge> {
//   late Vector2 velocity = Vector2.zero();
//   bool isMoving = false;
//   late final RectangleHitbox hitBox;
//   late final SpriteAnimationTicker animationTickerS;
//   late final SpriteAnimationTicker animationTickerK;
//   late AnimatedPlayerItem mainHandItem;
//
//   Player2()
//       : super(
//           size: CharConfig.characterSize * 0.7,
//           anchor: Anchor.center,
//           position: Vector2(0, 0),
//         );
//
//   @override
//   void onLoad() {
//     _loadSprites();
//
//     current = PlayerState.knife;
//
//     // mainHandItem = Sword();
//
//     super.onLoad();
//   }
//
//   @override
//   void update(dt) {
//     current = game.playerState;
//
//     animationTickerS.update(dt);
//
//     super.update(dt);
//   }
//
//   // void foo() {
//   //   final SpriteAnimationTicker? ticker =
//   //       animations?.values.elementAt(game.testIndex).createTicker()
//   //         ?..onComplete = () {
//   //           game.playerState = PlayerState.stand;
//   //         };
//   // }
//
//   Future<void> _loadSprites() async {
//     final characterSprite =
//         Sprite(game.images.fromCache(CharImg.TEST_CHARACTER));
//
//     final animationSword = SpriteAnimationData.variable(
//         amount: 5,
//         stepTimes: [0.2, 0.1, 0.1, 0.1, 0.3],
//         loop: false,
//         textureSize: Vector2(320, 480));
//
//     final animationKnife = SpriteAnimationData.variable(
//       amount: 5,
//       stepTimes: [0.2, 0.1, 0.1, 0.1, 0.3],
//       textureSize: Vector2(260, 360),
//     );
//
//     final animationS = SpriteAnimation.fromFrameData(
//       game.images.fromCache(Animations.sword),
//       animationSword,
//     );
//     final animationK = SpriteAnimation.fromFrameData(
//       game.images.fromCache(Animations.knife),
//       animationKnife,
//     );
//
//     animationTickerS = SpriteAnimationTicker(animationS);
//     animationTickerK = SpriteAnimationTicker(animationK);
//
//     animations = {
//       PlayerState.sword: animationS,
//       PlayerState.knife: animationK,
//       PlayerState.stand: SpriteAnimation.spriteList([characterSprite],
//           stepTime: 1, loop: false)
//     };
//   }
// }
