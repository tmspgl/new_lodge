import 'package:flutter/material.dart';

import '../../../settings.dart';
import '../../character.dart';

class CharacterPreview extends StatelessWidget {
  final Character character;

  const CharacterPreview({super.key, required this.character});

  String getHairColor() {
    if (character.hairStyle == HairStyle.long) {
      return CharImg.hairColorsLong[character.hairColor]!;
    } else {
      return CharImg.hairColorsShort[character.hairColor]!;
    }
  }

  String getDressColorPrimary() {
    if (character.dressStyle == DressStyle.skirt) {
      return CharImg.dressSkirtPrimaryColors[character.dressColorPrimary]!;
    } else {
      return CharImg.dressPantsPrimaryColors[character.dressColorPrimary]!;
    }
  }

  String getDressColorSecondary() {
    if (character.dressStyle == DressStyle.skirt) {
      return CharImg.dressSkirtSecondaryColors[character.dressColorSecondary]!;
    } else {
      return CharImg.dressPantsSecondaryColors[character.dressColorSecondary]!;
    }
  }

  List getCharacterImageCompose() {
    final char = [];
    char.addAll([
      Image.asset(CharImg.outlineHead),
      Positioned(child: Image.asset(CharImg.hairStyles[character.hairStyle]!)),
      Positioned(
          child: Image.asset(CharImg.dressStyles[character.dressStyle]!)),
      Positioned(child: Image.asset(CharImg.skinTones[character.skinTone]!)),
      Positioned(child: Image.asset(CharImg.eyeColors[character.eyeColor]!)),
      Positioned(child: Image.asset(getHairColor())),
      Positioned(child: Image.asset(getDressColorPrimary())),
      Positioned(child: Image.asset(getDressColorSecondary())),
    ]);
    return char;
  }

  List<Widget> getCharacterImageCompose2() {
    final List<Widget> char = [];
    char.addAll([
      Image.asset(CharImg.outlineHead),
      Image.asset(CharImg.hairStyles[character.hairStyle]!),
      Image.asset(CharImg.dressStyles[character.dressStyle]!),
      Image.asset(CharImg.skinTones[character.skinTone]!),
      Image.asset(CharImg.eyeColors[character.eyeColor]!),
      Image.asset(getHairColor()),
      Image.asset(getDressColorPrimary()),
      Image.asset(getDressColorSecondary()),
    ]);
    return char;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 300,
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(20),
      color: Colors.blue,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: 400,
          ),
          Positioned(
            right: 50,
            child: Stack(
              children: getCharacterImageCompose2(),
            ),
          ),
          Positioned(
            right: 20,
            bottom: 135,
            child: Image.asset(
              ToolImg.miniSword,
              fit: BoxFit.contain,
            ),
          ),
          Positioned(
            right: 80,
            bottom: -45,
            child: Image.asset(
              ToolImg.miniSword2,
              fit: BoxFit.contain,
            ),
          ),

          // Positioned(
          //   child: Stack(
          //     children: getCharacterImageCompose2(),
          //   ),
          // ),
        ],
      ),
    );
  }
}
