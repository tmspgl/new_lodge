import 'package:flame/components.dart';
import 'package:flutter/material.dart';
import 'package:new_lodge/game/new_lodge.dart';

import '../../character.dart';
import 'character_preview.dart';

const Map<CreationStage, CreationStage?> previousCreationState = {
  CreationStage.skinTone: null,
  CreationStage.eyeColor: CreationStage.skinTone,
  CreationStage.hairStyle: CreationStage.eyeColor,
  CreationStage.hairColor: CreationStage.hairStyle,
  CreationStage.dressStyle: CreationStage.hairColor,
  CreationStage.dressColorPrimary: CreationStage.dressStyle,
  CreationStage.dressColorSecondary: CreationStage.dressColorPrimary,
};

const Map<CreationStage, CreationStage?> nextCreationState = {
  CreationStage.skinTone: CreationStage.eyeColor,
  CreationStage.eyeColor: CreationStage.hairStyle,
  CreationStage.hairStyle: CreationStage.hairColor,
  CreationStage.hairColor: CreationStage.dressStyle,
  CreationStage.dressStyle: CreationStage.dressColorPrimary,
  CreationStage.dressColorPrimary: CreationStage.dressColorSecondary,
  CreationStage.dressColorSecondary: null,
};

const Map<CreationStage, List<dynamic>> creationStateValues = {
  CreationStage.skinTone: SkinTone.values,
  CreationStage.eyeColor: EyeColor.values,
  CreationStage.hairStyle: HairStyle.values,
  CreationStage.hairColor: HairColor.values,
  CreationStage.dressStyle: DressStyle.values,
  CreationStage.dressColorPrimary: DressColorPrimary.values,
  CreationStage.dressColorSecondary: DressColorSecondary.values,
};

List<CreationStage> stages = [
  CreationStage.skinTone,
  CreationStage.eyeColor,
  CreationStage.hairStyle,
  CreationStage.hairColor,
  CreationStage.dressStyle,
  CreationStage.dressColorPrimary,
  CreationStage.dressColorSecondary,
];

enum CreationStage {
  skinTone('Skin Tone'),
  eyeColor('Eye Color'),
  hairStyle('Hair Style'),
  hairColor('Hair Color'),
  dressStyle('Dress Style'),
  dressColorPrimary('Dress Brightness'),
  dressColorSecondary('Dress Color');

  const CreationStage(this.displayText);

  final String displayText;
}

class CharacterCreation extends StatefulWidget {
  final NewLodge game;

  const CharacterCreation({super.key, required this.game});

  @override
  State<StatefulWidget> createState() => _CharacterCreationState();
}

class _CharacterCreationState extends State<CharacterCreation> {
  late final Character character;
  late CreationStage creationState;

  @override
  void initState() {
    character = Character(
      SkinTone.light,
      EyeColor.green,
      HairStyle.long,
      HairColor.green,
      DressStyle.skirt,
      DressColorPrimary.dark,
      DressColorSecondary.green,
    );
    creationState = CreationStage.skinTone;

    super.initState();
  }

  void changeValue(List<dynamic> valueList, int currentIndex) {
    int next;
    if (currentIndex == valueList.length - 1) {
      next = 0;
    } else {
      next = currentIndex + 1;
    }
    setState(() {
      character.skinTone = valueList[next];
    });
  }

  void nextValue() {
    int next;
    switch (creationState) {
      case CreationStage.skinTone:
        final currentIndex = character.skinTone.index;
        if (currentIndex == SkinTone.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.skinTone = SkinTone.values[next];
        });
        break;
      case CreationStage.eyeColor:
        final currentIndex = character.eyeColor.index;
        if (currentIndex == EyeColor.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.eyeColor = EyeColor.values[next];
        });
        break;
      case CreationStage.hairStyle:
        final currentIndex = character.hairStyle.index;
        if (currentIndex == HairStyle.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.hairStyle = HairStyle.values[next];
        });
        break;
      case CreationStage.hairColor:
        final currentIndex = character.hairColor.index;
        if (currentIndex == HairColor.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.hairColor = HairColor.values[next];
        });
        break;
      case CreationStage.dressStyle:
        final currentIndex = character.dressStyle.index;
        if (currentIndex == DressStyle.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.dressStyle = DressStyle.values[next];
        });
        break;
      case CreationStage.dressColorPrimary:
        final currentIndex = character.dressColorPrimary.index;
        if (currentIndex == DressColorPrimary.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.dressColorPrimary = DressColorPrimary.values[next];
        });
        break;
      case CreationStage.dressColorSecondary:
        final currentIndex = character.dressColorSecondary.index;
        if (currentIndex == DressColorSecondary.values.length - 1) {
          next = 0;
        } else {
          next = currentIndex + 1;
        }
        setState(() {
          character.dressColorSecondary = DressColorSecondary.values[next];
        });
        break;
    }
  }

  void previousValue() {
    int previous;
    switch (creationState) {
      case CreationStage.skinTone:
        final currentIndex = character.skinTone.index;
        if (currentIndex == 0) {
          previous = SkinTone.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.skinTone = SkinTone.values[previous];
        });
        break;
      case CreationStage.eyeColor:
        final currentIndex = character.eyeColor.index;
        if (currentIndex == 0) {
          previous = EyeColor.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.eyeColor = EyeColor.values[previous];
        });
        break;
      case CreationStage.hairStyle:
        final currentIndex = character.hairStyle.index;
        if (currentIndex == 0) {
          previous = HairStyle.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.hairStyle = HairStyle.values[previous];
        });
        break;
      case CreationStage.hairColor:
        final currentIndex = character.hairColor.index;
        if (currentIndex == 0) {
          previous = HairColor.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.hairColor = HairColor.values[previous];
        });
        break;
      case CreationStage.dressStyle:
        final currentIndex = character.dressStyle.index;
        if (currentIndex == 0) {
          previous = DressStyle.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.dressStyle = DressStyle.values[previous];
        });
        break;
      case CreationStage.dressColorPrimary:
        final currentIndex = character.dressColorPrimary.index;
        if (currentIndex == 0) {
          previous = DressColorPrimary.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.dressColorPrimary = DressColorPrimary.values[previous];
        });
        break;
      case CreationStage.dressColorSecondary:
        final currentIndex = character.dressColorSecondary.index;
        if (currentIndex == 0) {
          previous = DressColorSecondary.values.length - 1;
        } else {
          previous = currentIndex - 1;
        }
        setState(() {
          character.dressColorSecondary = DressColorSecondary.values[previous];
        });
        break;
    }
  }

  void setNextCreationState() {
    final next = nextCreationState[creationState];
    if (next != null) {
      setState(() {
        creationState = next;
      });
    }
  }

  void setPreviousCreationState() {
    final previous = previousCreationState[creationState];
    if (previous != null) {
      setState(() {
        creationState = previous;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    const blackTextColor = Color.fromRGBO(0, 0, 0, 1.0);
    const whiteTextColor = Color.fromRGBO(255, 255, 255, 1.0);

    return Material(
      color: Colors.transparent,
      child: Center(
          child: Container(
        // padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(
          left: 20,
          right: 20,
          top: 30,
          bottom: 20,
        ),
        decoration: const BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.all(
            Radius.circular(0),
          ),
        ),
        child: Container(
          padding: const EdgeInsets.all(0.0),
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Character Creation',
                style: TextStyle(
                  color: whiteTextColor,
                  fontSize: 24,
                ),
              ),
              const SizedBox(height: 40),
              CharacterPreview(
                character: character,
              ),
              const SizedBox(height: 20),
              Text(
                creationState.displayText,
                style: const TextStyle(
                  color: whiteTextColor,
                  fontSize: 20,
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      previousValue();
                    },
                    iconSize: 60,
                    icon: Image.asset('assets/images/hud/icons/arrow_left.png'),
                  ),
                  const SizedBox(
                    width: 50,
                  ),
                  IconButton(
                    onPressed: () {
                      nextValue();
                    },
                    iconSize: 60,
                    icon:
                        Image.asset('assets/images/hud/icons/arrow_right.png'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      widget.game.overlays.remove('character_creation');
                    },
                    child: const Text('close'),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setPreviousCreationState();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: whiteTextColor,
                    ),
                    child: Text(
                      previousCreationState[creationState]?.displayText ?? '',
                      style: const TextStyle(
                        fontSize: 20.0,
                        color: blackTextColor,
                      ),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setNextCreationState();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: whiteTextColor,
                    ),
                    child: Text(
                      nextCreationState[creationState]?.displayText ?? '',
                      style: const TextStyle(
                        fontSize: 20.0,
                        color: blackTextColor,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 0),
            ],
          ),
        ),
      )),
    );
  }
}
