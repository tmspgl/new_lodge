// import 'package:flame/components.dart';
// import 'package:flame/parallax.dart';
// import 'package:new_lodge/game/new_lodge.dart';
//
// class MyWorld extends ParallaxComponent<NewLodge> {
//   @override
//   Future<void> onLoad() async {
//     anchor = Anchor.topLeft;
//     parallax = await game.loadParallax(
//       [
//         ParallaxImageData(ImageAssets.worldBackground),
//       ],
//       fill: LayerFill.height,
//       repeat: ImageRepeat.repeat,
//       // alignment: Alignment.center,
//       baseVelocity: Vector2(0, 0),
//       velocityMultiplierDelta: Vector2(1, 1),
//     );
//   }
//
//   @override
//   void update(dt) {
//     final speed = Settings.backgroundSpeed.roundToDouble();
//     switch (game.input) {
//       case Direction.left:
//         parallax?.baseVelocity = Vector2(-speed, 0);
//         break;
//       case Direction.right:
//         parallax?.baseVelocity = Vector2(speed, 0);
//         break;
//       case Direction.up:
//         parallax?.baseVelocity = Vector2(0, -speed);
//         break;
//       case Direction.down:
//         parallax?.baseVelocity = Vector2(0, speed);
//         break;
//       case Direction.none:
//         parallax?.baseVelocity = Vector2(0, 0);
//         break;
//     }
//
//     super.update(dt);
//   }
// }