class Character {
  SkinTone skinTone;
  EyeColor eyeColor;
  HairStyle hairStyle;
  HairColor hairColor;
  DressStyle dressStyle;
  DressColorPrimary dressColorPrimary;
  DressColorSecondary dressColorSecondary;

  Character(this.skinTone, this.eyeColor, this.hairStyle, this.hairColor,
      this.dressStyle, this.dressColorPrimary, this.dressColorSecondary);
}

enum SkinTone { light, tanned, medium, dark }

enum EyeColor { green, blue, brown }

enum HairStyle { long, short }

enum HairColor { red, green, blond, brown, black }

enum DressStyle { skirt, pants }

enum DressColorPrimary { dark, light }

enum DressColorSecondary { green, blue, red }
