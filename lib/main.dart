import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:new_lodge/game/testing_hud.dart';

import 'game/initialization/character_creation/character_creation.dart';
import 'game/new_lodge.dart';

void main() {
  runApp(GameWidget.controlled(
    gameFactory: NewLodge.new,
    overlayBuilderMap: {
      'character_creation': (_, NewLodge game) {
        return CharacterCreation(game: game);
      },
      'test_hud': (ctx, NewLodge game) {
        return testAnimationHudBuilder(ctx, game);
      }
    },
    initialActiveOverlays: const ['test_hud'],
  ));
}
