import 'package:flame/components.dart';

import 'game/character.dart';

//
enum Orientation { east, south, west, north }

enum Direction {
  none,
  north,
  northeast,
  east,
  southeast,
  south,
  southwest,
  west,
  northwest,
}

class Settings {
  static const frameTimeFps30 = 0.033;
  static const frameTimeSword = 0.7 / 5;
  static const frameTimeKnife = 0.5 / 4;

  // overlay id for menu
  static const menuOverlayId = 'GameMenu';
}

class Dimensions {
  // current char img 220x290. => x350
  // - Add some empty border for later (e.g. wearables).
  // - Weapons/Tool do not have to stay within this rect.
  static final characterBaseSize = Vector2(300, 350);

  static final characterDisplaySize = Vector2(80, 120);
}

class JoystickImg {
  static const background = 'hud/controls/joystick/background.png';
  static const knob = 'hud/controls/joystick/knob.png';
}

class CharImg {
  static const outlineHead = 'assets/images/character/outline/outline_head.png';

  static const outlineHairLong =
      'assets/images/character/outline/hair/outline_hair_01.png';
  static const outlineHairShort =
      'assets/images/character/outline/hair/outline_hair_02.png';

  static const outlineBodySkirt =
      'assets/images/character/outline/body/outline_body_01.png';
  static const outlineBodyPants =
      'assets/images/character/outline/body/outline_body_02.png';

  static const skinToneLight =
      'assets/images/character/color/skin/color_skin_01.png';
  static const skinToneTanned =
      'assets/images/character/color/skin/color_skin_02.png';
  static const skinToneMedium =
      'assets/images/character/color/skin/color_skin_03.png';
  static const skinToneDark =
      'assets/images/character/color/skin/color_skin_04.png';

  static const eyeColorGreen =
      'assets/images/character/color/eyes/color_eyes_01.png';
  static const eyeColorBlue =
      'assets/images/character/color/eyes/color_eyes_02.png';
  static const eyeColorBrown =
      'assets/images/character/color/eyes/color_eyes_03.png';

  static const hairLongColorRed =
      'assets/images/character/color/hair/01/color_hair_01_01.png';
  static const hairLongColorGreen =
      'assets/images/character/color/hair/01/color_hair_01_02.png';
  static const hairLongColorBlond =
      'assets/images/character/color/hair/01/color_hair_01_03.png';
  static const hairLongColorBrown =
      'assets/images/character/color/hair/01/color_hair_01_04.png';
  static const hairLongColorBlack =
      'assets/images/character/color/hair/01/color_hair_01_05.png';

  static const hairShortColorRed =
      'assets/images/character/color/hair/02/color_hair_02_01.png';
  static const hairShortColorGreen =
      'assets/images/character/color/hair/02/color_hair_02_02.png';
  static const hairShortColorBlond =
      'assets/images/character/color/hair/02/color_hair_02_03.png';
  static const hairShortColorBrown =
      'assets/images/character/color/hair/02/color_hair_02_04.png';
  static const hairShortColorBlack =
      'assets/images/character/color/hair/02/color_hair_02_05.png';

  static const dressSkirtColorPrimaryDark =
      'assets/images/character/color/body/01/primary/color_body_01_primary_01.png';
  static const dressSkirtColorPrimaryLight =
      'assets/images/character/color/body/01/primary/color_body_01_primary_02.png';

  static const dressPantsColorPrimaryDark =
      'assets/images/character/color/body/02/primary/color_body_02_primary_01.png';
  static const dressPantsColorPrimaryLight =
      'assets/images/character/color/body/02/primary/color_body_02_primary_02.png';

  static const dressSkirtColorSecondaryGreen =
      'assets/images/character/color/body/01/secondary/color_body_01_secondary_01.png';
  static const dressSkirtColorSecondaryBlue =
      'assets/images/character/color/body/01/secondary/color_body_01_secondary_02.png';
  static const dressSkirtColorSecondaryRed =
      'assets/images/character/color/body/01/secondary/color_body_01_secondary_03.png';

  static const dressPantsColorSecondaryGreen =
      'assets/images/character/color/body/02/secondary/color_body_02_secondary_01.png';
  static const dressPantsColorSecondaryBlue =
      'assets/images/character/color/body/02/secondary/color_body_02_secondary_02.png';
  static const dressPantsColorSecondaryRed =
      'assets/images/character/color/body/02/secondary/color_body_02_secondary_03.png';

  // TODO remove later
  static const TEST_CHARACTER = 'test_character.png';

  static const Map<HairStyle, String> hairStyles = {
    HairStyle.long: CharImg.outlineHairLong,
    HairStyle.short: CharImg.outlineHairShort,
  };

  static const Map<DressStyle, String> dressStyles = {
    DressStyle.skirt: CharImg.outlineBodySkirt,
    DressStyle.pants: CharImg.outlineBodyPants,
  };

  static const Map<SkinTone, String> skinTones = {
    SkinTone.light: CharImg.skinToneLight,
    SkinTone.tanned: CharImg.skinToneTanned,
    SkinTone.medium: CharImg.skinToneMedium,
    SkinTone.dark: CharImg.skinToneDark,
  };

  static const Map<EyeColor, String> eyeColors = {
    EyeColor.green: CharImg.eyeColorGreen,
    EyeColor.blue: CharImg.eyeColorBlue,
    EyeColor.brown: CharImg.eyeColorBrown,
  };

  static const Map<HairColor, String> hairColorsLong = {
    HairColor.red: CharImg.hairLongColorRed,
    HairColor.green: CharImg.hairLongColorGreen,
    HairColor.blond: CharImg.hairLongColorBlond,
    HairColor.brown: CharImg.hairLongColorBrown,
    HairColor.black: CharImg.hairLongColorBlack,
  };

  static const Map<HairColor, String> hairColorsShort = {
    HairColor.red: CharImg.hairShortColorRed,
    HairColor.green: CharImg.hairShortColorGreen,
    HairColor.blond: CharImg.hairShortColorBlond,
    HairColor.brown: CharImg.hairShortColorBrown,
    HairColor.black: CharImg.hairShortColorBlack,
  };

  static const Map<DressColorPrimary, String> dressSkirtPrimaryColors = {
    DressColorPrimary.dark: CharImg.dressSkirtColorPrimaryDark,
    DressColorPrimary.light: CharImg.dressSkirtColorPrimaryLight,
  };

  static const Map<DressColorPrimary, String> dressPantsPrimaryColors = {
    DressColorPrimary.dark: CharImg.dressPantsColorPrimaryDark,
    DressColorPrimary.light: CharImg.dressPantsColorPrimaryLight,
  };

  static const Map<DressColorSecondary, String> dressSkirtSecondaryColors = {
    DressColorSecondary.green: CharImg.dressSkirtColorSecondaryGreen,
    DressColorSecondary.blue: CharImg.dressSkirtColorSecondaryBlue,
    DressColorSecondary.red: CharImg.dressSkirtColorSecondaryRed,
  };

  static const Map<DressColorSecondary, String> dressPantsSecondaryColors = {
    DressColorSecondary.green: CharImg.dressPantsColorSecondaryGreen,
    DressColorSecondary.blue: CharImg.dressPantsColorSecondaryBlue,
    DressColorSecondary.red: CharImg.dressPantsColorSecondaryRed,
  };
}

class ToolImg {
  static const sword = 'assets/images/tools/sword_gold_long.png';
  static const miniSword = 'assets/images/tools/mini_sword.png';
  static const miniSword2 = 'assets/images/tools/mini_sword_02.png';
}

class Animations {
  static const sword = 'animation/sword.png';
  static const knife = 'animation/knife.png';
  static const stand = 'animation/stand.png';

  static const playerStand = 'animation/player/stand.png';
}
